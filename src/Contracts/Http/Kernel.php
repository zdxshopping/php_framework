<?php
namespace Asf\Contracts\Http;

interface Kernel
{
    public function handle($request);
}
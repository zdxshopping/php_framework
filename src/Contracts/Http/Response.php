<?php
namespace Asf\Contracts\Http;

interface Response
{
    public function makeupSwooleResponse($request);
}
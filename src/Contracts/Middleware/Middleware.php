<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/7
 * Time: 10:58
 */
namespace Asf\Contracts\Middleware;

/**
 * 中间件接口，定义中间件必须继承该接口
 * Interface Middleware
 * @package Asf\Contracts\Middleware
 */
interface Middleware
{
    public function handle($request, $redis);
}
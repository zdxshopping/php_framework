<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/6
 * Time: 11:05
 */
namespace Asf\Contracts\Queue;

interface MessageQueue
{
    public function handle($data):bool;
}
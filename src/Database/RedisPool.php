<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/2
 * Time: 16:44
 */
namespace Asf\Database;

use Swoole\Database\RedisConfig;
use Swoole\Database\RedisPool as Pool;
use Swoole\Coroutine;
class RedisPool
{
    public $pool;
    public function __construct()
    {
        $this->pool = new Pool((new RedisConfig)
            ->withHost('127.0.0.1')
            ->withPort(6379)
            ->withAuth('')
            ->withDbIndex(0)
            ->withTimeout(1),
            100
        );
    }

    public function connect()
    {
        $redis = $this->pool->get();
        return $redis;
    }

    public function close($redis)
    {
        $this->pool->put($redis);
    }

}
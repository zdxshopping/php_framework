<?php


namespace Asf\Foundation;


use Asf\Foundation\Http\Kernel;

class Server
{
    protected $address;

    protected $port;

    protected $server;

    public function __construct($address, $port)
    {
        $this->address = $address;
        $this->port = $port;
        $this->server = new \Swoole\Http\Server($this->address, $this->port);
    }

    public function handleRequest(callable $callback)
    {
        $this->server->on('request', function ($request, $response) use ($callback) {
            \Swoole\Coroutine::create($callback, $request, $response);
        });
    }

    public function on($event, $callback)
    {
        $this->server->on($event, $callback);
    }

    public function run()
    {
        $this->server->start();
    }
}
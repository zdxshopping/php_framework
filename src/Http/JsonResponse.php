<?php
namespace Asf\Http;

class JsonResponse extends \Symfony\Component\HttpFoundation\JsonResponse implements \Asf\Contracts\Http\Response
{
    use MakeupSwooleResponse;
}
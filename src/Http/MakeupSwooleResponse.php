<?php
namespace Asf\Http;

use Asf\Util\Context;
use DI\Container;

trait MakeupSwooleResponse
{
    public function makeupSwooleResponse($response)
    {
        $response->status($this->getStatusCode());

        foreach ($this->headers->allPreserveCaseWithoutCookies() as $name => $values) {
            $replace = 0 === strcasecmp($name, 'Content-Type');
            foreach ($values as $value) {
                $response->header($name, $value, $replace);
            }
        }

        return $this->getContent();
    }

//$response->status($this->getStatusCode());
//
//foreach ($this->headers->allPreserveCaseWithoutCookies() as $name => $values) {
//$replace = 0 === strcasecmp($name, 'Content-Type');
//foreach ($values as $value) {
//$response->header($name, $value, $replace);
//}
//}
//
///** @var Container $container */
//$container = Context::get('container');
//
///** @var Request $request */
//$request = $container->get(Request::class);
//
//if (! $request->cookies->has($request->getSession()->getName())) {
//    $response->cookie(
//        $request->getSession()->getName(),
//        $request->getSession()->getId(),
//        ini_get('session.cookie_lifetime') + time(),
//        ini_get('session.cookie_path'),
//        ini_get('session.cookie_domain'),
//        $request->isSecure(),
//        ini_get('session.cookie_httponly')
//    );
//}
//
//return $this->getContent();
}
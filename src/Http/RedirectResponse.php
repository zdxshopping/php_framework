<?php

namespace Asf\Http;

class RedirectResponse extends \Symfony\Component\HttpFoundation\RedirectResponse implements \Asf\Contracts\Http\Response
{
    use MakeupSwooleResponse;
}
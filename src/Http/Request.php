<?php
namespace Asf\Http;


class Request extends \Symfony\Component\HttpFoundation\Request
{
    //通过Swoole提供的Request对象创建第三方库Request实例
    public function createFromSwooleRequest($request)
    {
        $server = array_merge($request->header, $request->server);
        $keys = array_keys($server);
        array_walk($keys, function (&$item) {
            $item = strtoupper($item);
        });
        $server = array_combine($keys, array_values($server));

        return new static(
            $request->get ?? [],
            $request->post ?? [],
            [],
            $request->cookie ?? [],
            $request->files ?? [],
            $server ?? [],
            $request->rawContent() ?? []
        );
    }
}
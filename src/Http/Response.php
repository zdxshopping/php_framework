<?php
namespace Asf\Http;

class Response extends \Symfony\Component\HttpFoundation\Response implements \Asf\Contracts\Http\Response
{
    use MakeupSwooleResponse;
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/6
 * Time: 13:56
 */
namespace Asf\MessageQueue;

use Asf\Database\RedisPool;

/**
 * 消息队列生产者类，增加待处理的队列供消费者处理
 * Class MessageProducer
 * @package Asf\MessageQueue
 */
class MessageProducer
{
    protected $redis;
    protected $key;
    public function __construct(RedisPool $redisPool)
    {
        $this->redis = $redisPool;
    }

    /**
     * 设置生产者数据，外部应使用该方法设置
     * @param string $key 增加队列redis对应的标识，必须是config配置文件下queue数组下的其中的某个key
     * @param array $data 要处理的数据，要发送邮件的邮箱的数组集合等等
     * @return $this
     */
    public function set($key, $data)
    {
        $this->key = $key;
        $this->addMessage([$key => $data]);
        return $this;
    }

    /**
     * 延迟队列
     * @param int $time  单位秒
     * @param string $key 延迟队列的key，与配置文件中一样
     * @param string $data 要传给监听处理的内容
     */
    public function delay($key, $data, $time){
        $redis = $this->redis->connect();
        $redis->zadd($key, $time, $data);
        $this->redis->close($redis);
    }

    /**
     * 设置生产者数据，该方法对function set();进一步封装
     * @param array $data 要添加的生产者消息队列数据
     */
    protected function addMessage($data)
    {
        foreach ($data as $k => $v)
        {
            $redis = $this->redis->connect();
            $redis->lPush($k, ...$v);
            $this->redis->close($redis);
        }
    }
}
<?php
namespace Asf\Routing;

use Asf\Http\Request;
use Asf\Util\Context;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

class Router
{
    protected $container;

    public function __construct()
    {
        $this->container = Context::get('container');
    }

    /**
     * @param  Request $request
     * @return Route
     */
    public function matchRoute($request)
    {
        //通过容器获取路由器
        $dispatcher = $this->container->get(GroupCountBased::class);
        $uri = $request->getPathInfo() ? $request->getPathInfo() : '/';

        //返回路由对象
        return new Route($dispatcher->dispatch($request->getMethod(), $uri));
    }
}
<?php

use Asf\Support\Env;
use Twig\Environment;

if (! function_exists('value')) {
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (! function_exists('my_env')) {
    /**
     * Gets the value of an environment variable.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function my_env($key, $default = null)
    {
        return Env::get($key, $default);
    }
}

if (! function_exists('view')) {
    function view($templateName, $vars = [])
    {
        $container = \Asf\Util\Context::get('container');
        return $container->get(Environment::class)->render($templateName, $vars);
    }
}
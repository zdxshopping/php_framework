<?php

namespace Asf\Util;

use Swoole\Coroutine;

class Context
{
    //判断给定的key是否存在于上下文对象中
    public static function has($key)
    {
        return isset(self::getContext()[$key]);
    }

    //将给定的key=>value保存在上下文对象中
    public static function set($key, $value)
    {
        $context = self::getContext();
        $context[$key] = $value;
    }

    //从上下文对象中获取给定的key值
    public static function get($key)
    {
        $context = self::getContext();
        return $context[$key];
    }

    //获取协程上下文对象
    public static function getContext()
    {
        return Coroutine::getContext();
    }
}